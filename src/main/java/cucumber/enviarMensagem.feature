Feature: Enviar mensagem a outra pessoa por whatsapp 

Scenario: Enviar mensagem a outra pessoa de meus contatos pelo celular

	Given estou no whatsapp no aparelho celular configurado com meu numero de telefone
	And tenho o contato da pessoa já cadastrado
	When seleciono o contato
	Then o sistema exibe a tela para digitação da mensagem
	And eu digito a mensagem 
	And clico no botão para enviar
	

Scenario: Enviar mensagem a outra pessoa não cadastrada em meus contatos pelo celular
	
	Given estou no whatsapp no aparelho celular configurado com meu numero de telefone
	But não possuo o contato cadastrado em meu celular 
	When cadastro o novo contato
	And seleciono o contato cadastrado
	Then o sistema exibe a tela para digitação da mensagem
	And eu digito a mensagem 
	And clico no botão para enviar


