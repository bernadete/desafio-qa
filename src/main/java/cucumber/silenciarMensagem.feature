Feature: Silenciar mensagem de usuário por tempo determinado

Scenario: Silenciar mensagens de usuário por 8 horas com opção para exibir notificação

	Given estou no whatsapp no aparelho celular configurado com meu numero de telefone
	And tenho o contato da pessoa já cadastrado
	When seleciono o contato
	And seleciono a opção "Silenciar"
	Then o sistema exibe as opções de tempo para silenciar
	And seleciono a opção "8 Horas"
	And seleciono opção para "Exibir notificações"
	And seleciono a opção "ok" para finalizar a operação
	
	
Scenario: Silenciar mensagens de usuário por 1 ano com opção para Não exibir notificação

	Given estou no whatsapp no aparelho celular configurado com meu numero de telefone
	And tenho o contato da pessoa já cadastrado
	When seleciono o contato
	And seleciono a opção "Silenciar"
	Then o sistema exibe as opções de tempo para silenciar
	And seleciono a opção "1 Ano"
	And não seleciono a opção para "Exibir notificações"
	And seleciono a opção "ok" para finalizar a operação
	
	
Scenario: Cancelar o Silenciamento das mensagens de usuário por 1 semana 

	Given estou no whatsapp no aparelho celular configurado com meu número de telefone
	And tenho o contato da pessoa já cadastrado
	When seleciono o contato
	And seleciono a opção "Silenciar"
	Then o sistema exibe as opções de tempo para silenciar
	And seleciono a opção "1 Semana"
	And seleciono a opção "Cancelar"
	
		