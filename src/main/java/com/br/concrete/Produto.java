package com.br.concrete;

public class Produto {

	
	protected String nome;
	protected int idProduto;
	protected int quantidade;


	public Produto(String nome, int idProduto, int quantidade) {
		this.nome = nome;
		this.quantidade = quantidade;
		this.idProduto = idProduto;
	}

	public String getNome() {
		return nome;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public int getIdProduto() {
		return idProduto;
	}
	@Override
	public String toString() {
		return "[Produto: " + this.nome + "," + "Quantidade:" + this.quantidade + "]"; 
	}

	

	
}



