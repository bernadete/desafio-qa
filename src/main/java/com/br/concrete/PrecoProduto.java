package com.br.concrete;

public class PrecoProduto extends Produto {

	private double precoEspecial1 = 130.00;
	private double precoEspecial2 = 45.00;
	private double precoVendaFinal;
	private int n = 0;
	private double precos[] = new double[n];

	
	public PrecoProduto(String nome, int idProduto, int quantidade, double precoVendaFinal, int n, double[] precos) {
		super(nome, idProduto, quantidade);
		this.precoVendaFinal = precoVendaFinal;
		this.n = n;
		this.precos = precos;
	}


	public double getPrecoEspecial1() {
		return precoEspecial1;
	}

	public double getPrecoEspecial2() {
		return precoEspecial2;
	}

	public double getPrecoVendaFinal() {
		return precoVendaFinal;
	}

	// atribuindo precos unitarios dos produtos,conforme o id.
	public void atribuindoPrecoUnitarioPorIdProdutos() {
		int idProduto = 1;
		switch (idProduto) {
		case 1:
			if (idProduto == 1) {
				precos[0] = 50.00;
			}
			break;
		case 2:
			if (idProduto == 2) {
				precos[1] = 30.00;
			}
			break;
		case 3:
			if (idProduto == 3) {
				precos[2] = 20.00;
			}
			break;
		case 4:
			if (idProduto == 4) {
				precos[3] = 15.00;
			}
			break;

		default:
			System.out.println("Esta não é um código de produto válido");

		}
	}

	public void calculaPrecoProdutosConformeQuantidade() {

		// se o produto tem quantidade maior ou igual 1, ele calcula a quantidade * precounitario. Neste caso não importa o produto.
		if (getQuantidade() >= 1) {
			precoVendaFinal = precos[0] * getQuantidade();
		} else {
			// se o id do produto especifico tiver quantidade maior que 1, o sistema realiza o calculo do valor conforme a quantidade X produto.
			if (idProduto == 1) {
				// verifica se é múltiplo de 3
				for (int i = getQuantidade(); i <= 100; i++) {
					if (i % 3 == 0) {
						precoVendaFinal = (getQuantidade() / 3) * precoEspecial1; 																
					} else {
						System.out.println("Numero não é multiplo de 3");
					}
				}
			} else {
				System.out.println("O código do produto não é igual a 1");
			}

		}
		
		//cálculo idem ao if anterior, porém verifica se é multiplo de 2 e calcula da mesma forma.
		if (idProduto == 2) {
			for (int i = getQuantidade(); i <= 100; i++) {
				if (i % 2 == 0) {
					precoVendaFinal = (getQuantidade() / 2) * precoEspecial2; 
				} else {
					System.out.println("Numero não é multiplo de 2");
				}
			}
		} else {
			System.out.println("O código do produto não é igual a 2");
		}

	}
}